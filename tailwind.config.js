module.exports = {
  purge: [],
  theme: {
    extend: {
      fontFamily: {
        manrope: ['Manrope', 'sans-serif'],
      },
      translate: {
        '-1/2': '-50%',
      },
      inset: {
        50: '50%',
      },
      colors: {
        primary: { // charcoal
          100: '#9A9FA3',
          200: '#81868B',
          300: '#696E74',
          400: '#51565D',
          500: '#393E46',
          600: '#32363F',
          700: '#2B2F38',
          800: '#242730',
          900: '#1D1F29',
        },
        secondary: '#2BCED6', // dark turquoise
        white: '#ffffff',
        'white-0': '#FBFBFB',
        black: '#0D1B2A',
        transparent: 'transparent',
        focus: '#D5DDE4',
        blue: {
          100: '#5EE0E3',
          200: '#4CD6DA',
          300: '#39CCD1',
          400: '#13B7BE',
          500: '#00ADB5',
          600: '#009AA2',
          700: '#00878E',
          800: '#00747B',
        },
        danger: '#ff6b6b',
      },
    },
  },
  variants: {
    // order matters here
    backgroundColor: ['responsive', 'hover', 'focus', 'active'],
  },
  plugins: [],
};
