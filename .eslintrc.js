module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  extends: ["airbnb", "plugin:react/recommended", "react-app"],
  globals: {
    __PATH_PREFIX__: true,
    Atomics: "readonly",
    SharedArrayBuffer: "readonly",
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: "module",
  },
  plugins: ["react"],
  rules: {
    "react/jsx-filename-extension": [1, { extensions: [".js", ".jsx"] }],
    "block-spacing": "error",
    "sort-imports": ["error", {
      "ignoreCase": true
    }],
    "import/no-unresolved": [2, { ignore: ["^src/.+"] }],
    "no-unused-vars": ["error", { varsIgnorePattern: "jsx" }],
    "sort-imports": [2, {
      "ignoreCase": true,
      "ignoreMemberSort": false,
        "memberSyntaxSortOrder": ["none", "all", "multiple", "single"]
    }],
    'react/jsx-props-no-spreading': ['error', {
      html: 'enforce',
      custom: 'enforce',
      exceptions: ['StyledButton', 'StyledInput', 'StyledLink', 'AuthWrappedComponent'],
    }],
  },
};
