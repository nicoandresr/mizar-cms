/* eslint-disable no-unused-vars */
/* eslint-disable import/order */
import { API, graphqlOperation } from 'aws-amplify';
import PropTypes from 'prop-types';
import React from 'react';
import { useForm } from 'react-hook-form';

const BuildingCreator = ({ onCreation }) => {
  const { register, handleSubmit } = useForm();

  const onSubmit = ({ name }) => {
    const input = { name, logoUrl: '' };
    // API.graphql(graphqlOperation(createCompany, { input }));
    onCreation();
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} method="post">
      <label htmlFor="name">
        <h3>What is your building name</h3>
        <input
          id="name"
          name="name"
          placeholder="EX. Americas del tintal, Porvenir"
          type="text"
          ref={register}
        />
      </label>
    </form>
  );
};

BuildingCreator.propTypes = {
  onCreation: PropTypes.func,
};

BuildingCreator.defaultProps = {
  onCreation: () => {},
};

export default BuildingCreator;
