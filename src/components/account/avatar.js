import { AvatarDefaultIcon } from 'src/components/icons';
import PropTypes from 'prop-types';
import React from 'react';
import tw from 'twin.macro';

function Avatar({ src, altText }) {
  return src !== ''
    ? (
      <img
        css={tw`rounded-full box-content cursor-pointer m-0 p-4 w-12`}
        src={String(src)}
        alt={altText}
      />
    )
    : <AvatarDefaultIcon />;
}

Avatar.propTypes = {
  src: PropTypes.string,
  altText: PropTypes.string,
};

Avatar.defaultProps = {
  src: '',
  altText: 'icon',
};

export default Avatar;
