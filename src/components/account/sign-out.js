import { Auth } from 'aws-amplify';
import React from 'react';

const onSignOut = async () => {
  try {
    await Auth.signOut();
  } catch (error) {
    // TODO: Catch error
  }
};

const SignOut = () => (
  <button type="button" onClick={onSignOut}>Sign Out</button>
);

export default SignOut;
