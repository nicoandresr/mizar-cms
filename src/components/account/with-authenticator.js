import { PopUp, Spinner } from 'src/components/UI';
import React from 'react';
import { SignIn } from 'src/components/account';
import useUser from 'src/hooks/user.hook';

function withAuthenticator(AuthWrappedComponent) {
  const AppWithAuthenticator = (props) => {
    const [user] = useUser();

    if (!user) {
      return (
        <PopUp>
          <SignIn />
        </PopUp>
      );
    }

    if (user.isLoding) return <Spinner />;

    return <AuthWrappedComponent {...props} />;
  };

  return AppWithAuthenticator;
}

export default withAuthenticator;
