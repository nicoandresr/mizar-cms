export { default as Profile } from 'src/components/account/profile';
export { default as ResetPassword } from 'src/components/account/reset-password';
export { default as SignIn } from 'src/components/account/sign-in';
export { default as SignOut } from 'src/components/account/sign-out';
export { default as SignUp } from 'src/components/account/sign-up';
export { default as withAuthenticator } from 'src/components/account/with-authenticator';
export { default as Avatar } from 'src/components/account/avatar';
export { BoxProviders, ButtonGoogle } from 'src/components/account/providers';
