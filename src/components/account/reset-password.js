import * as UI from 'src/components/UI';
import { graphql, useStaticQuery } from 'gatsby';
import { Auth } from 'aws-amplify';
import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';
import tw from 'twin.macro';
import { useForm } from 'react-hook-form';

function ResetPassword({ onCancel }) {
  const { allLocalesYaml: { nodes: [{ resetCmp, singleNames }] } } = useStaticQuery(query);
  const i18 = { ...resetCmp, ...singleNames };

  const STEPS = {
    sendCode: 'sendCode',
    receiptCode: 'receiptCode',
  };

  const {
    register, handleSubmit, errors, formState,
  } = useForm();

  const [step, setStep] = React.useState(STEPS.sendCode);
  const [errorMsg, setErrorMsg] = React.useState(null);

  const onSendCodeSubmit = ({ username }) => {
    Auth.forgotPassword(username)
      .then(() => { setErrorMsg(null); setStep(STEPS.receiptCode); })
      .catch((error) => setErrorMsg(error.message));
  };

  const onResetPassSubmit = ({
    username,
    code,
    password,
  }) => {
    Auth.forgotPasswordSubmit(username, code, password)
      .then(() => { setErrorMsg(null); onCancel(); })
      .catch((error) => setErrorMsg(error.message));
  };


  const { isSubmitting } = formState;

  const sendCode = (
    <StyledForm onSubmit={handleSubmit(onSendCodeSubmit)}>
      <UI.Field>
        <UI.Label for="username">{i18.userName}</UI.Label>
        <UI.Input
          id="username"
          type="text"
          name="username"
          ref={register({ required: true })}
          error={errors.username}
        />
      </UI.Field>
      <StyledControls>
        <UI.Link isPrimary onClick={onCancel}>{i18.backToSignIn}</UI.Link>
        <UI.Button isPrimary type="submit">{i18.sendCode}</UI.Button>
      </StyledControls>
    </StyledForm>
  );

  const receiptCode = (
    <StyledForm onSubmit={handleSubmit(onResetPassSubmit)}>
      <UI.Field>
        <UI.Label for="username">{i18.userName}</UI.Label>
        <UI.Input
          id="username"
          type="text"
          name="username"
          ref={register({ required: true })}
          error={errors.username}
        />
      </UI.Field>
      <UI.Field>
        <UI.Label for="code">{i18.code}</UI.Label>
        <UI.Input
          id="code"
          type="text"
          name="code"
          ref={register({ required: true })}
          error={errors.code}
        />
      </UI.Field>
      <UI.Field>
        <UI.Label for="code">{i18.password}</UI.Label>
        <UI.Input
          id="password"
          type="password"
          name="password"
          ref={register({ required: true })}
          error={errors.password}
        />
      </UI.Field>
      <StyledControls>
        <UI.Link isPrimary onClick={onCancel}>{i18.backToSignIn}</UI.Link>
        <UI.Button isPrimary type="submit">{i18.confirm}</UI.Button>
      </StyledControls>
    </StyledForm>
  );

  return (
    <StyledContainer>
      <h2>{i18.headline}</h2>
      { step === STEPS.sendCode ? sendCode : receiptCode }
      {errorMsg && <StyledErrorMessage>{errorMsg}</StyledErrorMessage>}
      {isSubmitting && i18.submitting }
    </StyledContainer>
  );
}

const StyledContainer = styled.div`
  ${tw`w-56 flex-col min-w-full font-manrope w-full`}
`;

const StyledForm = styled.form`
  ${tw`flex-col`}

  & button[type="submit"] {
    ${tw`w-1/2`}
  }
`;

const StyledControls = styled.div`
  ${tw`flex items-center justify-between mt-8`}
`;

const StyledErrorMessage = styled.span`
  ${tw`w-full text-red-400 block mt-8 text-sm`}
`;

ResetPassword.propTypes = {
  onCancel: PropTypes.func,
};

ResetPassword.defaultProps = {
  onCancel: () => {},
};

export default ResetPassword;

const query = graphql`
  query ResetPasswordTranslations {
    allLocalesYaml {
      nodes {
        singleNames {
          userName,
          password,
          confirm,
          submitting
        },
        resetCmp {
          backToSignIn,
          sendCode
        }
      }
    }
  }
`;
