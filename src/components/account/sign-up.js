import {
  Button,
  Input,
  Label,
  Spinner,
} from 'src/components/UI';
import { ErrorMessage, useForm } from 'react-hook-form';
import { graphql, useStaticQuery } from 'gatsby';
import React, { useEffect } from 'react';
import { Auth } from 'aws-amplify';
import { BoxProviders } from 'src/components/account';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import tw from 'twin.macro';

const SignUp = ({ onSignUp }) => {
  const {
    register, handleSubmit, errors, formState,
  } = useForm();


  const { allLocalesYaml: { nodes: [{ signUpCmp, singleNames }] } } = useStaticQuery(query);
  const i18 = { ...signUpCmp, ...singleNames };

  const onSubmit = ({ username, password, email }) => Auth.signUp({
    username,
    password,
    attributes: {
      email,
    },
  });

  const { isSubmitted, isSubmitting, isValid } = formState;

  useEffect(() => {
    if (isSubmitted) {
      onSignUp();
    }
  }, [isSubmitted, onSignUp]);

  return (
    <StyledContainer>
      <h2>{i18.headline}</h2>
      <StyledForm onSubmit={handleSubmit(onSubmit)}>
        <Label for="email">Email</Label>
        <Input
          id="email"
          type="email"
          name="email"
          ref={register({ required: true })}
          error={errors.email}
        />
        <ErrorMessage errors={errors} name="email" message="This is required" />
        <Label for="password">Password</Label>
        <Input
          id="password"
          name="password"
          type="password"
          ref={register({ required: true })}
          error={errors.password}
        />
        <Label for="username">UserName</Label>
        <Input
          id="username"
          name="username"
          type="text"
          ref={register({ required: true })}
          error={errors.username}
        />
        <Button isPrimary type="submit">Sign up</Button>
      </StyledForm>
      { isSubmitted && isValid && <span>User registered</span> }
      { isSubmitting && <Spinner /> }
      <BoxProviders textBtnGoogle={i18.signUpWithGoogle} />
    </StyledContainer>
  );
};

SignUp.propTypes = {
  onSignUp: PropTypes.func,
};

SignUp.defaultProps = {
  onSignUp: () => {},
};

const StyledContainer = styled.div`
  ${tw`w-56 flex-col min-w-full font-manrope text-sm w-full`}
`;

const StyledForm = styled.form`
  ${tw`flex-col w-full`}

  & button[type="submit"] {
    ${tw`mt-8`}
  }

`;

export default SignUp;

const query = graphql`
  query SignUpTranslations {
    allLocalesYaml {
      nodes {
        singleNames {
          userName,
          password,
        }
        signUpCmp {
          signUpWithGoogle,
          headline,
        }
      }
    }
  }
`;
