import { Avatar } from 'src/components/account';
import React from 'react';
import styled from '@emotion/styled';
import tw from 'twin.macro';
import useCurrentUser from 'src/hooks/user.hook';

function Profile() {
  const [user] = useCurrentUser();
  return user ? (
    <StyledProfile>
      <StyledUserInfo>
        <h4>
          {user.userName}
        </h4>
        <span>Admon</span>
      </StyledUserInfo>
      <Avatar src={user.picture} />
    </StyledProfile>
  ) : null;
}

const StyledUserInfo = styled.div`
  ${tw`flex flex-col items-end text-right mr-5 font-manrope`}

  h4 {
    font-size: 16px;
    margin: 0;
  }

  span {
    font-size: 14px;
  }
`;

const StyledProfile = styled.div`
  ${tw`flex flex-row m-0 items-center relative`}
`;

export default Profile;
