import * as UI from 'src/components/UI';
import { BoxProviders, ResetPassword } from 'src/components/account';
import { graphql, useStaticQuery } from 'gatsby';
import { Auth } from 'aws-amplify';

import React from 'react';
import styled from '@emotion/styled';
import tw from 'twin.macro';
import { useForm } from 'react-hook-form';

function SignIn() {
  const {
    register, handleSubmit, errors, formState,
  } = useForm();

  const STEPS = {
    signIn: 'SignIn',
    forgotPassword: 'ForgotPassword',
  };

  const onSubmit = ({ username, password }) => Auth.signIn({
    username,
    password,
  });

  const { allLocalesYaml: { nodes: [{ signInCmp, singleNames }] } } = useStaticQuery(query);

  const { isSubmitting } = formState;

  const i18 = { ...signInCmp, ...singleNames };

  const [step, setStep] = React.useState(STEPS.signIn);

  const signIn = (
    <StyledContainer>
      <h2>{i18.headline}</h2>
      <StyledForm onSubmit={handleSubmit(onSubmit)}>
        <UI.Field>
          <UI.Label for="username">{i18.userName}</UI.Label>
          <UI.Input
            id="username"
            type="text"
            name="username"
            ref={register({ required: true })}
            error={errors.username}
          />
        </UI.Field>
        <UI.Field>
          <UI.Label for="password">{i18.password}</UI.Label>
          <UI.Input
            id="password"
            type="password"
            name="password"
            ref={register({ required: true })}
            error={errors.password}
          />
        </UI.Field>

        <StyledForgotPassLink>
          <span>
            {i18.forgotPasswordQuestion}
            {' '}
          </span>
          <UI.Link onClick={() => { setStep(STEPS.forgotPassword); }} isPrimary>
            {i18.resetPassword}
          </UI.Link>
        </StyledForgotPassLink>

        <UI.Button isPrimary type="submit">{i18.login}</UI.Button>
      </StyledForm>
      <BoxProviders textBtnGoogle={i18.signInWithGoogle} />
      {isSubmitting && <UI.Spinner />}
    </StyledContainer>
  );

  return (step === STEPS.signIn
    ? signIn
    : (<ResetPassword onCancel={() => setStep(STEPS.signIn)} />)
  );
}

const StyledContainer = styled.div`
  ${tw`w-56 flex-col min-w-full font-manrope text-sm w-full`}
`;

const StyledForm = styled.form`
  ${tw`flex-col`}

  & button[type="submit"] {
    ${tw`mt-8 w-full`}
  }
`;

const StyledForgotPassLink = styled.div`
  ${tw`mt-8 w-full`}
`;

export default SignIn;

const query = graphql`
  query SignInTranslations {
    allLocalesYaml {
      nodes {
        singleNames {
          userName,
          password,
          login,
          or
        }
        signInCmp {
          signInWithGoogle,
          forgotPasswordQuestion,
          resetPassword,
          headline,
        }
      }
    }
  }
`;
