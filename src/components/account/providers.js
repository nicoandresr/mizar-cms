import { graphql, useStaticQuery } from 'gatsby';
import { Auth } from 'aws-amplify';
import { AUTH_PROVIDERS } from 'src/constants';
import { GoogleIcon } from 'src/components/icons';
import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';
import tw from 'twin.macro';

function BoxProviders({ textBtnGoogle }) {
  const data = useStaticQuery(query);
  const { allLocalesYaml: { nodes: [{ singleNames: i18 }] } } = data;
  return (
    <StyledProviders>
      <p>
        <span>{i18.or}</span>
      </p>
      <ButtonGoogle text={textBtnGoogle} />
    </StyledProviders>
  );
}

BoxProviders.propTypes = {
  textBtnGoogle: PropTypes.string.isRequired,
};

function ButtonGoogle({ text }) {
  return (
    <StyledGoogleButton
      type="button"
      onClick={() => Auth.federatedSignIn({ provider: AUTH_PROVIDERS.google })}
    >
      <GoogleIcon />
      <span css={tw`ml-2`}>{text}</span>
    </StyledGoogleButton>
  );
}

ButtonGoogle.propTypes = {
  text: PropTypes.string.isRequired,
};

const StyledProviders = styled.div`s
  ${tw`mt-10`}

  p { ${tw`text-center text-sm`} }
  p > span {
    ::before,
    ::after {
      content: '';
      height: 1px;
      width: calc(50% - 24px);
      ${tw`block bg-gray-500 inline-block my-2`}
    }

    ::before {
      float: left;
    }

    ::after {
      float: right;
    }
  }
`;

const StyledGoogleButton = styled.button`
  ${tw`
    border border-gray-400 rounded px-16
    border-solid
    cursor-pointer
    flex flex-row
    font-manrope text-sm
    hover:bg-gray-100
    items-center
    justify-center
    py-1
    w-full bg-white
  `}
`;

const query = graphql`
  query ProvidersTranslations {
    allLocalesYaml {
      nodes {
        singleNames {
          or
        }
      }
    }
  }
`;

export { ButtonGoogle, BoxProviders };
