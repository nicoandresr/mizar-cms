import BuildingCreator from 'src/components/building/building-creator';
import React from 'react';
import { SignUp } from 'src/components/account';
import { useStep } from 'react-hooks-helper';

const BuildingCreation = () => {
  const { index, navigation: { next } } = useStep({ steps: 2 });

  const stepOne = (
    <div>
      <div>Step 1 of 2</div>
      <h2> New Account </h2>
      <SignUp onSignUp={next} />
    </div>
  );

  return (
    <div>
      { index === 0 && stepOne}
      { index === 1 && <BuildingCreator onCreation={next} />}
    </div>
  );
};

export default BuildingCreation;
