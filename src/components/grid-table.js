import React from 'react';
import styled from '@emotion/styled';
import tw from 'twin.macro';

function GridTable() {
  return (
    <StyledGrid>
      <tbody>
        <tr css={tw`text-center h-20`}>
          <td>No result found</td>
        </tr>
      </tbody>
    </StyledGrid>
  );
}

export default GridTable;

const StyledGrid = styled.table`
  ${tw`w-full bg-white-0 border-solid border-2 border-focus rounded-md`}
`;
