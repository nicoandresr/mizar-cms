export { default as HomeIcon } from 'src/components/icons/home';
export { default as MembersIcon } from 'src/components/icons/members';
export { default as ProfileIcon } from 'src/components/icons/profile';
export { default as GoogleIcon } from 'src/components/icons/google';
export { default as AvatarDefaultIcon } from 'src/components/icons/avatar-default';
