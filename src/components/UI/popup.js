import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';
import tw from 'twin.macro';

function PopUp({ children }) {
  return (
    <PopUpContainer>{children}</PopUpContainer>
  );
}

PopUp.propTypes = {
  children: PropTypes.element.isRequired,
};

const PopUpContainer = styled.div`
  ${tw`
    w-1/5 fixed
    left-50
    top-50
    transform
    -translate-x-1/2
    -translate-y-1/2
    shadow-2xl
    p-12
  `}
`;

export default PopUp;
