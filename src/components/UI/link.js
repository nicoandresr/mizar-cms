import React from 'react';
import styled from '@emotion/styled';
import tw from 'twin.macro';

const StyledLink = styled.a(({
  isPrimary, isSecondary,
}) => [
  tw`
    no-underline text-sm cursor-pointer
  `,

  isPrimary && tw`
    text-blue-500
  `,

  isSecondary && tw`
    text-gray-500
  `,

]);

const Link = (props) => <StyledLink {...props} />;

export default Link;
