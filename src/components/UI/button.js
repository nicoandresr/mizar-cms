import { css } from '@emotion/core';
import React from 'react';
import styled from '@emotion/styled';
import tw from 'twin.macro';

const StyledButton = styled.button(({
  isPrimary, isSecondary,
}) => [
  tw`
    block
    border-none
    cursor-pointer transition duration-300 ease-in-out
    p-3 rounded text-base`,

  isPrimary && tw`
    active:bg-blue-600
    bg-blue-500
    focus:outline-none focus:shadow-outline
    hover:bg-blue-400 hover:shadow-lg
    text-white`,

  isSecondary
    && css`
      box-shadow: 0 0.1em 0 0 rgba(0, 0, 0, 0.25);
      ${tw`border-2`}
    `,
]);

// eslint-disable-next-line react/jsx-props-no-spreading
const Button = (props) => <StyledButton {...props} />;

export default Button;
