import { keyframes } from '@emotion/core';
import React from 'react';
import styled from '@emotion/styled';

const rotate = keyframes`
  0% { transform: rotate(0) }
  100% { transform: rotate(360deg) }
`;

const SpinnerContainer = styled.div`
  width: 200px;
  height: 200px;
  display: inline-block;
  overflow: hidden;
  background: #ffffff;
`;

const SpinnerObject = styled.div`
  width: 100%;
  height: 100%;
  position: relative;
  transform: translateZ(0) scale(1);
  backface-visibility: hidden;
  transform-origin: 0 0;

  div { box-sizing: border-box!important }

  & > div {
    position: absolute;
    width: 112px;
    height: 112px;
    top: 44px;
    left: 44px;
    border-radius: 50%;
    border: 16px solid #000;
    border-color: #47F6FF transparent #47F6FF transparent;
    animation: ${rotate} 1.5s linear infinite;
  }

  & > div:nth-of-type(2), & > div:nth-of-type(4) {
    width: 76px;
    height: 76px;
    top: 62px;
    left: 62px;
    animation: ${rotate} 1.5s linear infinite reverse;
  }

  & > div:nth-of-type(2) {
    border-color: transparent #C3CAD5 transparent #C3CAD5
  }

  & > div:nth-of-type(3) { border-color: transparent }
  & > div:nth-of-type(3) div {
    position: absolute;
    width: 100%;
    height: 100%;
    transform: rotate(45deg);
  }

  & > div:nth-of-type(3) div:before, & > div:nth-of-type(3) div:after {
    content: "";
    display: block;
    position: absolute;
    width: 16px;
    height: 16px;
    top: -16px;
    left: 32px;
    background: #47F6FF;
    border-radius: 50%;
    box-shadow: 0 96px 0 0 #47F6FF;
  }

  & > div:nth-of-type(3) div:after {
    left: -16px;
    top: 32px;
    box-shadow: 96px 0 0 0 #47F6FF;
  }

  & > div:nth-of-type(4) { border-color: transparent; }

  & > div:nth-of-type(4) div {
    position: absolute;
    width: 100%;
    height: 100%;
    transform: rotate(45deg);
  }

  & > div:nth-of-type(4) div:before, & > div:nth-of-type(4) div:after {
    content: "";
    display: block;
    position: absolute;
    width: 16px;
    height: 16px;
    top: -16px;
    left: 14px;
    background: #C3CAD5;
    border-radius: 50%;
    box-shadow: 0 60px 0 0 #C3CAD5;
  }

  & > div:nth-of-type(4) div:after {
    left: -16px;
    top: 14px;
    box-shadow: 60px 0 0 0 #C3CAD5;
  }

  & div { box-sizing: content-box; }
`;

const Spinner = () => (
  <SpinnerContainer>
    <SpinnerObject>
      <div />
      <div />
      <div>
        <div />
      </div>
      <div>
        <div />
      </div>
    </SpinnerObject>
  </SpinnerContainer>
);

export default Spinner;
