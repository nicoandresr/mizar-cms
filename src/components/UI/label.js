import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';
import tw from 'twin.macro';

const Label = styled.label(() => [
  tw`my-2 lg:my-3 block text-primary-700 text-sm`,
]);

function StyledLabel({ children }) {
  return (
    <Label>
      {children}
    </Label>
  );
}

StyledLabel.propTypes = {
  children: PropTypes.string.isRequired,
};

export default StyledLabel;
