import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';
import tw from 'twin.macro';

const StyledInput = styled.input`
  box-sizing: border-box;
  ${tw`
    bg-gray-200
    border-0
    focus:bg-white focus:border-solid focus:border focus:border-gray-200
    outline-none
    p-3
    rounded
    w-full`}
`;

const InputError = styled.span`
  ${tw`my-1 text-danger block text-sm`}
`;

const Input = React.forwardRef((props, ref) => (
  <>
    <StyledInput {...props} ref={ref} />
    { props.error && <InputError>This field is required</InputError> }
  </>
));

Input.propTypes = {
  error: PropTypes.string,
};

Input.defaultProps = {
  error: null,
};

Input.displayName = 'StyledInput';

export default Input;
