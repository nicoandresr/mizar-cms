import PropTypes, { element } from 'prop-types';
import React from 'react';
import tw from 'twin.macro';

const Field = ({ children }) => (
  <div css={tw`mt-6`}>{children}</div>
);

Field.propTypes = {
  children: PropTypes.arrayOf(element).isRequired,
};

export default Field;
