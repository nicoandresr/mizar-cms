import { graphql, Link, useStaticQuery } from 'gatsby';
import { HomeIcon, MembersIcon } from 'src/components/icons';
import React from 'react';
import routes from 'src/routes';
import styled from '@emotion/styled';
import tw from 'twin.macro';

function NavBar() {
  const data = useStaticQuery(query);
  const { allLocalesYaml: { nodes: [{ singleNames: i18 }] } } = data;
  return (
    <StyledNavBar>
      <StyledFront>
        <StyledNav>
          <Link to={routes.home}><HomeIcon /></Link>

          <Link to={routes.members}><MembersIcon /></Link>
        </StyledNav>
      </StyledFront>

      <div css={tw`relative`}>
        <StyledFront data-ui-id="nav-drawner">
          <StyledNavDrawer>
            <StyledLink to={routes.home}>{i18.dashboard}</StyledLink>

            <StyledLink to={routes.members}>{i18.members}</StyledLink>
          </StyledNavDrawer>

        </StyledFront>
      </div>
    </StyledNavBar>
  );
}

export default NavBar;

const query = graphql`
  query myQueryAndMyQuery {
    allLocalesYaml {
      nodes {
        singleNames {
          dashboard
          members
        }
      }
    }
  }
`;

const StyledFront = styled.div`
  ${tw`flex flex-col h-screen z-20 bg-primary-600`}
`;

const StyledLink = styled(Link)`
  ${tw`no-underline text-white`}
`;

const StyledNav = styled.nav`
  ${tw`flex flex-col items-center justify-around m-auto w-16 h-40`}
`;

const StyledNavDrawer = styled(StyledNav)`
  ${tw`items-baseline w-full`}
`;

const StyledNavBar = styled.div`
  ${tw`flex`}

  & div[data-ui-id="nav-drawner"] {
    ${tw`z-10 absolute w-48`}
    transform: translateX(-100%);
    transition: transform 0.3s;
  }

  &:hover div[data-ui-id="nav-drawner"] {
    transform: translateX(0);
  }
`;
