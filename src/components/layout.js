import { css, Global } from '@emotion/core';
import NavDrawer from 'src/components/nav-drawer';
import Profile from 'src/components/account/profile';
import PropTypes from 'prop-types';
import React from 'react';
import tw from 'twin.macro';
import { withAuthenticator } from 'src/components/account';

const propTypes = {
  children: PropTypes.node.isRequired,
};

function Layout({ children }) {
  return (
    <>
      <Global styles={globalStyles} />
      <Profile />
      <div css={tw`flex`}>
        <NavDrawer />

        <main css={tw`mx-10 my-5 w-full`}>{children}</main>
      </div>
    </>
  );
}

Layout.propTypes = propTypes;

export default withAuthenticator(Layout);

const globalStyles = css`
  body {
    ${tw`m-0 font-manrope`}
  }
`;
