import { Auth, Hub } from 'aws-amplify';
import React from 'react';
import UserCognito from 'src/auth/userCognito.class';

const AUTH_CHANNEL = 'auth';

async function getCurrentUser() {
  try {
    const authenticatedUser = await Auth.currentAuthenticatedUser();
    if (authenticatedUser) {
      const { userDataKey, storage } = authenticatedUser;
      if (storage[userDataKey]) {
        return new UserCognito(storage[userDataKey]);
      }
    }

    return undefined;
  } catch {
    return undefined;
  }
}

function registerAuthListener(action) {
  Hub.listen(AUTH_CHANNEL, (bag) => {
    const { event } = bag.payload;

    if (!action[event]) return;

    action[event](bag);
  });
}

export default function useUser() {
  const [user, setUser] = React.useState();

  React.useEffect(() => {
    registerAuthListener({
      signIn: (bag) => {
        const { userDataKey, storage } = bag.payload.data;
        if (storage[userDataKey]) {
          setUser(new UserCognito(storage[userDataKey]));
        }
      },
    });

    registerAuthListener({
      signOut: () => {
        setUser(undefined);
      },
    });

    (async () => {
      const userData = await getCurrentUser();
      if (!userData) return;
      setUser(userData);
    })();
  }, []);

  function updateUser(userData) {
    setUser({ ...userData });
  }

  return [user, updateUser];
}
