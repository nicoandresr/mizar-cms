export default class UserCognito {
  constructor(userDataCognito) {
    this.rawUserData = JSON.parse(userDataCognito);
    const userInfo = this.rawUserData.UserAttributes.reduce(
      (acc, { Name: name, Value: value }) => {
        acc[name] = name !== 'identities' ? value : JSON.parse(value)[0];
        return acc;
      },
    );

    return { ...userInfo };
  }

  get userName() {
    const [userName] = (this.email || '@').split('@');
    return userName;
  }
}
