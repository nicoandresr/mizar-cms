import Layout from 'src/components/layout';
import React from 'react';
import { SignOut } from 'src/components/account';

function Dashboard() {
  return (
    <Layout>
      <h1>Welcome to Dashboard!</h1>
      <SignOut />
    </Layout>
  );
}

export default Dashboard;
