import GridTable from 'src/components/grid-table';
import Layout from 'src/components/layout';
import React from 'react';

function MemberList() {
  return (
    <Layout>
      <h1>Integrantes</h1>

      <GridTable />
    </Layout>
  );
}

export default MemberList;
