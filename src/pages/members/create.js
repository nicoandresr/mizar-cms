import Layout from 'src/components/layout';
import React from 'react';
import { useForm } from 'react-hook-form';

function MemberList() {
  const { errors, handleSubmit, register } = useForm();
  const [name, setName] = React.useState('');

  function onSubmit(data) {
    setName(data.name);
  }

  return (
    <Layout>
      <form onSubmit={handleSubmit(onSubmit)}>
        <input name="name" ref={register({ required: true })} />
        {errors.name && <span>Name is required</span>}
        <input type="submit" />
      </form>
      <p>
        Hello!
        {` ${name}`}
      </p>
    </Layout>
  );
}

export default MemberList;
