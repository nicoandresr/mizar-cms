import { css, Global } from '@emotion/core';
import { graphql, useStaticQuery } from 'gatsby';
import BuildingCreation from 'src/components/journeys/building-creation';
import Img from 'gatsby-image';
import React from 'react';
import tw from 'twin.macro';

function Company() {
  const buildingImg = useStaticQuery(graphql`
    query {
      file(relativePath: {eq: "appartment.png"}) {
        childImageSharp {
          fluid(maxWidth: 500) {
            ...GatsbyImageSharpFluid_withWebp_tracedSVG
          }
        }
      }
    }`);
  return (
    <div css={tw`grid grid-cols-12 grid-rows-1 gap-6 max-h-screen`}>
      <Global styles={
        css`
          body {
            ${tw`m-0 font-manrope`}
          }
        `
      }
      />
      <div css={tw`col-span-6`}>
        <Img fluid={buildingImg.file.childImageSharp.fluid} />
      </div>
      <div css={tw`col-start-8 col-end-12 flex flex-col items-center justify-center`}>
        <BuildingCreation />
      </div>
    </div>
  );
}

export default Company;
