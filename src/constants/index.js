export const AUTH_EVENTS = {
  signIn: 'signIn',
  signOut: 'signOut',
  signInFailure: 'signIn_failure',
  cognitoHostedUI: 'cognitoHostedUI',
};

export const CURRENT_USER_ATTRIBUTES = 'CURRENT_USER_ATTRIBUTES';

export const AUTH_PROVIDERS = {
  google: 'Google',
};
